from flask import Flask,render_template, request
import pandas
import numpy
import pickle
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
import csv
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, GlobalMaxPool1D, Conv1D, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.python.keras import regularizers
from tensorflow.keras.models import load_model


app = Flask(__name__)
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))
def decontract(sentence):
    sentence = re.sub(r"n\'t", " not", sentence)
    sentence = re.sub(r"\'re", " are", sentence)
    sentence = re.sub(r"\'s", " is", sentence)
    sentence = re.sub(r"\'d", " would", sentence)
    sentence = re.sub(r"\'ll", " will", sentence)
    sentence = re.sub(r"\'t", " not", sentence)
    sentence = re.sub(r"\'ve", " have", sentence)
    sentence = re.sub(r"\'m", " am", sentence)
    return sentence

def removePunctuation(sentence):
    sentence = re.sub(r'[?|!|\'|"|#]',r'',sentence)
    sentence = re.sub(r'[.|,|)|(|\|/]',r' ',sentence)
    sentence = sentence.strip()
    sentence = sentence.replace("\n"," ")
    return sentence

def removeNumber(sentence):
    alpha_sent = ""
    for word in sentence.split():
        alpha_word = re.sub('[^a-z A-Z]+', '', word)
        alpha_sent += alpha_word
        alpha_sent += " "
    alpha_sent = alpha_sent.strip()
    return alpha_sent

# function to remove stopwords
stop_words = set(stopwords.words('english'))
def remove_stopwords(text):
    no_stopword_text = [w for w in text.split() if not w in stop_words]
    return ' '.join(no_stopword_text)


def stemming(sentence):
    stemmer = SnowballStemmer("english")
    stemmedSentence = ""
    for word in sentence.split():
        stem = stemmer.stem(word)
        stemmedSentence += stem
        stemmedSentence += " "
    stemmedSentence = stemmedSentence.strip()
    return stemmedSentence

def clean_synopsis(df):
    df['clean_synopsis'] = df['synopsis'].apply(lambda x: decontract(x))
    df['clean_synopsis'] = df['clean_synopsis'].apply(lambda x: removePunctuation(x))
    df['clean_synopsis'] = df['clean_synopsis'].apply(lambda x: removeNumber(x))
    df['clean_synopsis'] = df['clean_synopsis'].apply(lambda x: remove_stopwords(x))
    df['clean_synopsis'] = df['clean_synopsis'].apply(lambda x: stemming(x))
    return df

def normalize_value(list):
    for row in list:
        index = sorted(range(len(row)), key=lambda i: row[i])[-5:]
        for i in range(0, len(row)):
            if i in index:
                row[i] = 1
            else:
                row[i]= 0
    return list
model_name = 'cnn_model.h5'
tokenizer_name = 'tokenizer.pickle'
column_names = 'column_names.pkl'
maxlen = 200

@app.route('/genres/train', methods=['GET','POST'])
def index():
    return render_template('train.html')

@app.route('/genres/predict', methods=['GET','POST'])
def predict():

    if request.method == 'POST':
        f = request.form['train_data']
        movie_genre = []
        with open(f, encoding="utf8") as file:
            csvfile = csv.reader(file)
            for row in csvfile:
                movie_genre.append(row)
        print("Data Fetched")
        movie_genre = pandas.DataFrame(movie_genre[1:],columns=movie_genre[0])
        label_df = movie_genre['genres'].str.get_dummies(' ')
        num_output = len(label_df.columns)
        print("Pre processing data")
        movie_genre = clean_synopsis(movie_genre)
        print("Total No of labels: {0}".format(num_output))
        synopsis_train, synopsis_val, y_train, y_val = train_test_split(movie_genre['clean_synopsis'], label_df,
        test_size=0.3, random_state=42, shuffle=True)
        print("Tokennizing Synopsis..")
        tokenizer = Tokenizer(num_words=5000, lower=True)
        tokenizer.fit_on_texts(synopsis_train)
        vocab_size = len(tokenizer.word_index) + 1
        filter_length = 256
        X_train = tokenizer.texts_to_sequences(synopsis_train)
        X_val = tokenizer.texts_to_sequences(synopsis_val)
        X_train = pad_sequences(X_train, padding='post', maxlen=maxlen)
        X_val = pad_sequences(X_val, padding='post', maxlen=maxlen)
        print("Creating CNN Model...")
        model = Sequential()
        model.add(Embedding(vocab_size, 100, input_length=maxlen))
        model.add(Conv1D(filter_length, 3, padding='valid', activation='relu', strides=1))
        model.add(GlobalMaxPool1D())
        model.add(Dense(100, activation = 'relu'))
        model.add(Dropout(0.5))
        model.add(Dense(num_output, activation='sigmoid'))
        # kernel_regularizer=regularizers.l2(0.03)
        # model.compile(optimizer=Adam(0.015), loss='binary_crossentropy', metrics=[tf.keras.metrics.AUC()])
        model.compile(optimizer=Adam(0.01), loss='binary_crossentropy', metrics=['acc'])
        print("Model is being trained..Please wait..")
        model.fit(X_train, y_train, batch_size=256, epochs=10, verbose=1, validation_data=(X_val, y_val))
        # pickle.dump(model, open(model_name, 'wb'))
        print('Saving Model..')
        model.save(model_name)
        with open(tokenizer_name, 'wb') as handle:
            pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open(column_names, 'wb') as f:
            pickle.dump(label_df.columns, f)
        return render_template('predict.html')
@app.route('/genres/predictions', methods=['GET','POST'])
def data():
    # model = pickle.load(open(model_name, 'rb'))
    model = load_model(model_name)
    tokenizer = pickle.load(open(tokenizer_name, 'rb'))
    label_columns = pickle.load(open(column_names, 'rb'))
    if request.method == 'POST':
        f = request.form['test_data']
        test_df = []
        with open(f, encoding="utf8") as file:
            csvfile = csv.reader(file)
            for row in csvfile:
                test_df.append(row)
        test_df = pandas.DataFrame(test_df[1:],columns=test_df[0])
        test_df = clean_synopsis(test_df)
        X_test = tokenizer.texts_to_sequences(test_df['clean_synopsis'])
        X_test = pad_sequences(X_test, padding='post', maxlen=maxlen)
        y_pred = model.predict(X_test)
        y_pred = normalize_value(y_pred)
        pred_df = pandas.DataFrame(y_pred,columns=[label_columns])
        pred_df = pred_df.apply(numpy.int64)
        def get_genre(row):
            genre = []
            for i in label_columns:
                if row[i].values == 1:
                    genre.append(i)
            genre = ' '.join(genre)
            return genre
        test_df['predicted_genres'] = pred_df.apply(get_genre, axis=1)
        test_df = test_df.loc[:, ['movie_id', 'synopsis', 'predicted_genres']]
        test_df.to_csv('submission.csv')
        return "Predictions saved.."

if __name__ == '__main__':
    app.run(debug=True)
